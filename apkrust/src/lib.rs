#![allow(non_camel_case_types)]

use md5::digest::generic_array::GenericArray;
use md5::{Digest, Md5};
use sha1::Sha1;
use sha2::{Sha256, Sha512};
use std::ffi::{c_char, c_void};
use std::ffi::{c_int, CStr};
use std::slice;

type c_size_t = usize;

// longest is SHA512
pub const APK_DIGEST_MAX_LENGTH: usize = 64;

macro_rules! c_try {
    ($e:expr, $err:expr) => {
        match $e {
            Ok(data) => data,
            Err(()) => return $err,
        }
    };
}

#[repr(C)]
#[allow(unused)]
enum APKE {
    EOF = 1024,
    DNS,
    URL_FORMAT,
    CRYPTO_ERROR,
    CRYPTO_NOT_SUPPORTED,
    CRYPTO_KEY_FORMAT,
    SIGNATURE_FAIL,
    SIGNATURE_UNTRUSTED,
    SIGNATURE_INVALID,
    FORMAT_NOT_SUPPORTED,
    PKGVERSION_FORMAT,
    DEPENDENCY_FORMAT,
    ADB_COMPRESSION,
    ADB_HEADER,
    ADB_VERSION,
    ADB_SCHEMA,
    ADB_BLOCK,
    ADB_SIGNATURE,
    ADB_NO_FROMSTRING,
    ADB_LIMIT,
    ADB_PACKAGE_FORMAT,
    V2DB_FORMAT,
    V2PKG_FORMAT,
    V2PKG_INTEGRITY,
    V2NDX_FORMAT,
    PACKAGE_NOT_FOUND,
    INDEX_STALE,
    FILE_INTEGRITY,
    CACHE_NOT_AVAILABLE,
    UVOL_NOT_AVAILABLE,
    UVOL_ERROR,
    UVOL_ROOT,
    REMOTE_IO,
}

#[repr(C)]
#[derive(Debug, Eq, PartialEq)]
pub struct apk_digest {
    alg: u8,
    len: u8,
    data: [u8; APK_DIGEST_MAX_LENGTH],
}

enum DigestAlgorithm {
    None,
    MD5,
    SHA1,
    SHA256,
    SHA512,
    SHA256_160,
}

impl DigestAlgorithm {
    pub(crate) fn alg_len(&self) -> u8 {
        (match self {
            DigestAlgorithm::None => 0usize,
            DigestAlgorithm::MD5 => Md5::output_size(),
            DigestAlgorithm::SHA1 => Sha1::output_size(),
            // It appears that SHA256/160 is just SHA256 truncated from 32 bytes to 20
            DigestAlgorithm::SHA256_160 => 20,
            DigestAlgorithm::SHA256 => Sha256::output_size(),
            DigestAlgorithm::SHA512 => Sha512::output_size(),
        }) as u8
    }
}

impl DigestAlgorithm {
    const fn as_c_str(&self) -> &'static CStr {
        let alg_str: &[u8] = match self {
            DigestAlgorithm::None => b"none\0",
            DigestAlgorithm::MD5 => b"md5\0",
            DigestAlgorithm::SHA1 => b"sha1\0",
            DigestAlgorithm::SHA256_160 => b"sha256-160\0",
            DigestAlgorithm::SHA256 => b"sha256\0",
            DigestAlgorithm::SHA512 => b"sha512\0",
        };

        // SAFETY: alg_str must be NUL terminated
        unsafe { CStr::from_bytes_with_nul_unchecked(alg_str) }
    }
}

impl TryFrom<u8> for DigestAlgorithm {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(DigestAlgorithm::None),
            1 => Ok(DigestAlgorithm::MD5),
            2 => Ok(DigestAlgorithm::SHA1),
            3 => Ok(DigestAlgorithm::SHA256),
            4 => Ok(DigestAlgorithm::SHA512),
            5 => Ok(DigestAlgorithm::SHA256_160),
            _ => Err(()),
        }
    }
}

#[no_mangle]
pub extern "C" fn apk_digest_alg_str(alg: u8) -> *const c_char {
    DigestAlgorithm::try_from(alg)
        .map(|alg| alg.as_c_str())
        .unwrap_or(unsafe { CStr::from_bytes_with_nul_unchecked(b"unknown\0") })
        .as_ptr()
}

#[no_mangle]
pub extern "C" fn apk_digest_alg_len(alg: u8) -> c_int {
    DigestAlgorithm::try_from(alg)
        .map(|alg| alg.alg_len().into())
        .unwrap_or(0)
}

// static inline int apk_digest_calc(struct apk_digest *d, uint8_t alg, const void *ptr, size_t sz)
#[no_mangle]
pub extern "C" fn apk_digest_calc_rust(
    d: *mut apk_digest,
    alg: u8,
    data: *const c_void,
    sz: c_size_t,
) -> c_int {
    const CRYPTO_ERR: c_int = -(APKE::CRYPTO_ERROR as c_int);

    assert!(!data.is_null());
    assert!(!d.is_null());
    let d = unsafe { &mut (*d) };

    let digest_alg = c_try!(alg.try_into(), CRYPTO_ERR);
    let data = unsafe { slice::from_raw_parts::<u8>(data as *const u8, sz) };
    match digest_alg {
        DigestAlgorithm::None => return CRYPTO_ERR,
        DigestAlgorithm::MD5 => {
            let mut h = Md5::new();
            h.update(data);
            h.finalize_into(GenericArray::from_mut_slice(
                &mut d.data[..Md5::output_size()],
            ));
        }
        DigestAlgorithm::SHA1 => {
            let mut h = Sha1::new();
            h.update(data);
            h.finalize_into(GenericArray::from_mut_slice(
                &mut d.data[..Sha1::output_size()],
            ));
        }
        DigestAlgorithm::SHA256_160 | DigestAlgorithm::SHA256 => {
            let mut h = Sha256::new();
            h.update(data);
            h.finalize_into(GenericArray::from_mut_slice(
                &mut d.data[..Sha256::output_size()],
            ));
        }
        DigestAlgorithm::SHA512 => {
            let mut h = Sha512::new();
            h.update(data);
            h.finalize_into(GenericArray::from_mut_slice(
                &mut d.data[..Sha512::output_size()],
            ));
        }
    }

    d.alg = alg;
    d.len = digest_alg.alg_len();

    0
}

#[no_mangle]
pub extern "C" fn apk_digest_compare(c: *const apk_digest, rust: *const apk_digest) {
    let c = unsafe { &*c };
    let rust = unsafe { &*rust };
    if c != rust {
        println!("C: {:?}\nnot equal to Rust:{:?}", c, rust);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn apk_digest_new() -> apk_digest {
        apk_digest {
            alg: 0,
            len: 0,
            data: [0; APK_DIGEST_MAX_LENGTH],
        }
    }

    #[test]
    fn apk_digest_max_length() {
        assert_eq!(APK_DIGEST_MAX_LENGTH, Sha512::output_size())
    }

    #[test]
    fn apk_digest_calc_md5() {
        let mut d = apk_digest_new();
        let data = "this is a test";
        assert_eq!(
            apk_digest_calc(
                &mut d as *mut _,
                DigestAlgorithm::MD5 as u8,
                data.as_ptr() as *const _,
                data.len()
            ),
            0
        );
        assert_eq!(
            &d.data[..d.len.into()],
            &[
                0x54, 0xb0, 0xc5, 0x8c, 0x7c, 0xe9, 0xf2, 0xa8, 0xb5, 0x51, 0x35, 0x11, 0x02, 0xee,
                0x09, 0x38
            ]
        );
    }

    #[test]
    fn apk_digest_calc_sha1() {
        let mut d = apk_digest_new();
        let data = "this is a test";
        assert_eq!(
            apk_digest_calc(
                &mut d as *mut _,
                DigestAlgorithm::SHA1 as u8,
                data.as_ptr() as *const _,
                data.len()
            ),
            0
        );
        assert_eq!(
            &d.data[..d.len.into()],
            &[
                0xfa, 0x26, 0xbe, 0x19, 0xde, 0x6b, 0xff, 0x93, 0xf7, 0x0b, 0xc2, 0x30, 0x84, 0x34,
                0xe4, 0xa4, 0x40, 0xbb, 0xad, 0x02
            ]
        );
    }

    #[test]
    fn apk_digest_calc_sha256_160() {
        let mut d = apk_digest_new();
        let data = "this is a test";
        assert_eq!(
            apk_digest_calc(
                &mut d as *mut _,
                DigestAlgorithm::SHA256_160 as u8,
                data.as_ptr() as *const _,
                data.len()
            ),
            0
        );
        assert_eq!(
            &d.data[..d.len.into()],
            &[
                0x2e, 0x99, 0x75, 0x85, 0x48, 0x97, 0x2a, 0x8e, 0x88, 0x22, 0xad, 0x47, 0xfa, 0x10,
                0x17, 0xff, 0x72, 0xf0, 0x6f, 0x3f, 0xf6, 0xa0, 0x16, 0x85, 0x1f, 0x45, 0xc3, 0x98,
                0x73, 0x2b, 0xc5, 0x0c
            ][..20]
        );
    }

    #[test]
    fn apk_digest_calc_sha256() {
        let mut d = apk_digest_new();
        let data = "this is a test";
        assert_eq!(
            apk_digest_calc(
                &mut d as *mut _,
                DigestAlgorithm::SHA256 as u8,
                data.as_ptr() as *const _,
                data.len()
            ),
            0
        );
        assert_eq!(
            &d.data[..d.len.into()],
            &[
                0x2e, 0x99, 0x75, 0x85, 0x48, 0x97, 0x2a, 0x8e, 0x88, 0x22, 0xad, 0x47, 0xfa, 0x10,
                0x17, 0xff, 0x72, 0xf0, 0x6f, 0x3f, 0xf6, 0xa0, 0x16, 0x85, 0x1f, 0x45, 0xc3, 0x98,
                0x73, 0x2b, 0xc5, 0x0c
            ]
        );
    }

    #[test]
    fn apk_digest_calc_sha512() {
        let mut d = apk_digest_new();
        let data = "this is a test";
        assert_eq!(
            apk_digest_calc(
                &mut d as *mut _,
                DigestAlgorithm::SHA512 as u8,
                data.as_ptr() as *const _,
                data.len()
            ),
            0
        );
        assert_eq!(
            &d.data[..d.len.into()],
            &[
                0x7d, 0x0a, 0x84, 0x68, 0xed, 0x22, 0x04, 0x00, 0xc0, 0xb8, 0xe6, 0xf3, 0x35, 0xba,
                0xa7, 0xe0, 0x70, 0xce, 0x88, 0x0a, 0x37, 0xe2, 0xac, 0x59, 0x95, 0xb9, 0xa9, 0x7b,
                0x80, 0x90, 0x26, 0xde, 0x62, 0x6d, 0xa6, 0x36, 0xac, 0x73, 0x65, 0x24, 0x9b, 0xb9,
                0x74, 0xc7, 0x19, 0xed, 0xf5, 0x43, 0xb5, 0x2e, 0xd2, 0x86, 0x64, 0x6f, 0x43, 0x7d,
                0xc7, 0xf8, 0x10, 0xcc, 0x20, 0x68, 0x37, 0x5c
            ]
        );
    }
}
